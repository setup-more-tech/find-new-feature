from flask import Flask, request, jsonify
import json
import requests
from flask_cors import CORS
from kafka import KafkaConsumer, KafkaProducer
app = Flask(__name__)
TOPIC_NAME = "FEATURES"
KAFKA_SERVER = "localhost:9092"

producer = KafkaProducer(
    bootstrap_servers = KAFKA_SERVER,
    api_version = (0, 11, 15)
)

@app.route('/kafka/pushFeature', methods=['POST'])
def kafkaProducer():
		
    req = request.get_json()
    json_payload = json.dumps(req)
    json_payload = str.encode(json_payload)
    producer.send(TOPIC_NAME, json_payload)
    producer.flush()
    print("Sent to consumer")
    #print(req)
    return jsonify({
        "message": "The feature has been created. We'll let you know once it's done", 
        "status": "Pass"})

if __name__ == "__main__":
    app.run(debug=True, port = 5000)